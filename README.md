BDrello: easily export, import and edit your Trello boards
==========================================================

Purpose
-------

BDrello is the perfect companion to use with Trello if you prefer textual
rather than web/mouse interfaces.

Bdrello has 3 main options:

  * **get** (fetch) a Trello board on standard output
  * **put** (update) a Trello board from standard input
  * **edit**, which means fetch a Trello board, edit it with your
    favorite text editor, and update it on the server

The BD language
---------------

Bdrello uses you a language named BD, loosely inspired from the Braindump
format used by the now defunct Hiveminder service. For example, here
is the BD of a typical Trello board issued by the command **bdrello get**:

```
    L(7f2) today
      C(fcd) Complete the bdrello program [perso]
        I would like to terminate bdrello
        since I have a need for such a
        tool.
        K(a2a) check
          [x] complete program
          [x] finish writing README.md
          [ ] finish writing the manual
          [x] complete LICENSE.md
          [ ] announce to the world
      C(a72) Buy milk and butter [perso]

    L() tomorrow
      C(f06) Do annoying stuff [work] [due: 2024-03-08:20:00.000Z]
      C() Do another boring stuff [work]
      C(9a2) Buy airplaine tickets [perso hollidays]

    L(f84) done
```

By editing this text you can add, destroy, modify or move any object,
and send your modifications back to the Trello server using the
command **bdrello put**.

You can also use **bdrello edit** to get, edit and push your
modifications in one simple command.

Limitations
-----------

There are some BD language limitations to consider for your Trello boards.
You cannot have:

  * spaces in label names
  * attachments in cards
  * reminders [TODO]
  * different members in a board [TODO]

How to use
----------

See BDrello manual ([bdrello.pdf](https://gitlab.com/pdagog/bdrello/-/raw/main/bdrello.pdf))

Installation
------------

Bdrello requires Python3 and the `request` package.
Moreover, you have to get an API key and a token to access the Trello
REST API (see BDrello manual).

To install, simply copy `bdrello.py` to your favorite binary location,
and `bdrello.man` to your favorite manual page location.

Author
------

Pierre David

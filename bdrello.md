% bdrello(1) user manual
% Pierre David

# NAME

bdrello - easy access to Trello boards via BD language

# SYNOPSIS

bdrello [*options*] [*action*]

# DESCRIPTION

Bdrello provides easy access to information on a Trello server: fetch
informations and send back changes. Taks and to-do-lists  are represented
with the BD language (see below), a very simple human-readable language,
which is editable with any text editor.

# ACTION

One of **get** (also abbreviated as **g**), **put** (also abbreviated
as **p**), **edit** (also abbreviated as **e**) or **list** (also
abbreviated as **l**)

* **get**: fetch the Trello board and print the BD on standard output
* **put**: read BD on standard input and send modifications to the Trello server
* **edit**: fetch the Trello board, launch editor, then send modifications to the Trello server
* **list**: list all Trello board names on standard output

Default: *get*

# OPTIONS

**-b** *name*, **\--board-name** *name*
:   Select the Trello board to work on.

    Default: name specified in the Bdrello configuration file (key `board`).

**-c** *path*, **\--config-file** *path*
:   Pathname of the configuration file (see below for the format).

    Default: **~/.config/bdrello**

**-d** *keys*, **\--debug** *keys*
:   Print detailed debug information about topics indicated as a comma
    separated list of keys. Available keys/topics are:

    key    topic
    -----  --------------------------------------------------------
    config configuration file analysis
    api    accesses to Trello REST API
    json   detail of objects returned by Trello REST API
    bd     parsed BD
    diff   differences between Trello board and the parsed BD

    By default, debug information is printed on standard error. This
    can be changed with the **-l** option below.

    Default: no debug information is printed

**-l** *path*, **\--log-file** *path*
:   File to use for debug information printing instead of standard error.

**-n**, **\--do-not-commit**
:   Do not send modifications to the Trello server. This option is
    often used with **-d api** to watch which API calls should be sent
    to the Trello server.

**-v**, **\--verbose**
:   Print additional information (number of objects) on standard output

**-V**, **\--version**
:   Print Bdrello version and exit.


**-h**, **\--help**
:   Display usage information.


# CONFIGURATION FILE

The `bdrello` configuration file is located in `~/.config/bdrello`,
unless another location is specified with the *-c* option. It contains
only one option `default`:

    [default]
	apiurl = https://api.trello.com/1
	apikey = xxx
	token = xxx
	board = my-preferred-board
	backupdir = ~/.bdrello

Available keys are:

  Key       Mandatory Role
  ----      --------- ------------------------------------------------------
  apiurl    optional  base URL of the Trello API server
  apikey    mandatory see below
  token     mandatory see below
  board     optional  default board name
  backupdir optional  when using the **edit** action, make a backup of the board in the local directory
  editor    optional  editor to use with the *edit* action

To get you very own **apikey** value,
go to <https://trello.com/power-ups/admin> and select [new], then fill some
of the fields (name, e-mail) and create your API key.
To get you very own **token** value,
go to <https://trello.com/power-ups/admin>, then to your API key and
select the [token] on the comment besides you API key.


# BD LANGUAGE

The BD language is a textual language loosely inspired by the braindump
of the now-defunct Hiveminder task manager. Here is an example of a BD
representing a Trello board:

    L(7f2) today
      C(fcd) Complete the bdrello program [perso]
        I would like to terminate bdrello
        since I have a need for such a
        tool.
        K(a2a) check
          [x] complete program
          [x] finish writing README.md
          [ ] finish writing the manual
          [x] complete LICENSE.md
          [ ] announce to the world
      C(a72) Buy milk and butter [perso]

    L() tomorrow
      C(f06) Do annoying stuff [work] [due: 2024-03-08:20:00.000Z]
      C() Do another boring stuff [work]
      C(9a2) Buy airplaine tickets [perso hollidays]

    L(f84) done

Indentation is used here (and in **bdrello get** output) for readability.
Except for card descriptions, spaces at the beginning of lines are
ignored.

Lines beginning with a **L** (list), a **C** (card) or a **K**
(checklist) introduce a Trello object. They are followed by an *id*
inside parenthesis: this *id* is a 3-characters summary of the real
Trello *id* which is much longer. In case of collision between different
Trello ids, BD *ids* may be longer than 3 characters. When moving objects
(for example to move a card between two lists), keep the *id* untouched:
in this example, the "Do annoying stuff" card has been moved from the
"today" to the "tomorrow" list, keeping the f06 id intact. If you want
to create a new object (for example the new "Do another boring stuff"
card), use an empty pair of parenthesis.

A card may include:

  * one or more labels: included in square brackets, as in "Buy airplane tickets"
  * a due time: introduced with **[due: ...]**
  * a description: following the **C** line and before any checklist

Checklist items are introduced with **[ ]** (exactly one character between
square brackets, without any *id*). Items are checked if one non-space
character resides between the square brackets (**[x]** in the example).

With this BD representation of a board, it is possible to destroy any
object by deleting the corresponding lines (except for Lists which can
only be archived, due to Trello API restriction), or to move objects
inside the enclosing object (e.g. move a card inside its list) or in
another object (e.g. move a card to another list).

# EXIT STATUS

This program returns 0 on success, and 1 if an error occurs.

# ENVIRONMENT

**EDITOR**
:   text editor to use with **edit** action, if no
    preferred editor has been specified in the Bdrello configuration file.

# EXAMPLES

List all my boards:

    $ bdrello list

Get the contents of board *foo* in the *bar.bd* file:

    $ bdrello -b foo get > bar.bd

Update the board *foo* on the server with my modifications:

    $ bdrello -b foo put < bar.bd

Show the calls which should be issued to the Trello REST API to
update my board, but do not execute these calls:

    $ bdrello -n -d api -b foo put < bar.bd

Get the current contents of my preferred board (which is configured in the
configuration file), edit the resulting BD and update the Trello server:

    $ bdrello edit

Edit the named board, and stores all API calls and detailed replies
(with JSON contents) into the /tmp/bdrello.log file:

    $ bdrello -l /tmp/bdrello.log -d api,json edit

# SEE ALSO

<https://gitlab.com/pdagog/bdrello>

#!/usr/bin/env python3

#
# Export, import, or edit Trello cards in your favorite editor
#
# You don't want to pull out your mouse and wait for your web
# browser to edit your Trello boards? You want to keep a copy
# of your tasks in a text file, and edit them with vi or emacs?
# Then, this program is for you.
#
# Examples of use:
# - bdrello list
#       list all your Trello boards
# - bdrello
#       send your default Trello board (configured in ~/.config/bdrello)
#       on stdout
# - bdrello -b foo -v put < foo.bd
#       send your modified 'foo' board (as it is in the foo.bd file)
#       to the Trello server, with verbose informations
# - bdrello -d api -b bar edit
#       launch the editor to edit the Trello 'bar' board, and
#       commit your modifications to the Trello server.
#       Additionally, display debug information on stderr
#       about calls to the API Trello server.
#
# Author: Pierre David pdagog <at> gmail.com
#

import sys
import os
import re
import json
import shutil
import argparse
import configparser
import subprocess
import datetime
import tempfile

# non standard module
try:
    import requests
except ImportError:
    print ("Python3 'requests' package required", file=sys.stderr)
    sys.exit (1)

BDRELLO_VERSION = 0.9

##############################################################################
# General BDrello class
##############################################################################

class BDrello:

    DEFAULT_EDITOR = 'vi'
    DEFAULT_URL = 'https://api.trello.com/1'

    ############################################################

    def __init__(self, conffile, debug, logfile):
        self._debug      = debug

        self._boards     = None         # all my boards

        self._lists      = None         # all lists in current board
        self._cards      = None         # all cards in current board
        self._checklists = None         # all checklists in current board
        self._labels     = None         # all labels in current board

        self._session    = requests.Session ()

        self._curname    = None         # current board name
        self._curbid     = None         # current board id

        self._id         = {}           # all Trello objects by id
        self._shid       = {}           # short ids attributed to Trello objects

        # Open logfile if there is one or more debug keys

        self._logfile    = sys.stderr
        if len(debug) > 0 and logfile is not None:
            try:
                self._logfile = open (logfile, 'w')
            except Exception as e:
                raise RuntimeError ('Cannot open {}\n{}'.format (logfile, e))

        # Read configuration file

        self.CONFIG      = os.path.expanduser ('~') + '/.config/bdrello'

        if conffile is None:
            conffile = self.CONFIG

        try:
            self._config = configparser.ConfigParser (interpolation=None)
            if not self._config.read (conffile):
                raise RuntimeError ('Cannot read file')

            self._d ('config', self._config)

            df = self._config ['default']
            self._apiurl       = df.get ('apiurl', BDrello.DEFAULT_URL)
            self._apikey       = df.get ('apikey')
            self._token        = df.get ('token')
            self._defaultboard = df.get ('board')
            self._backupdir    = df.get ('backupdir', None)
            self._editor       = df.get ('editor', None)

            if self._backupdir is not None:
                self._backupdir = os.path.expanduser (self._backupdir)

        except Exception as e:
            raise RuntimeError ('Error in configuration file {}\n{}'.
                                                    format (conffile, e))
        return

    ############################################################

    def _d (self, key, msg):
        """Display debug message if key is present in '-d' list

        :param key: debug key
        :param msg: message to be displayed
        """
        if key in self._debug:
            print ('{} {}'.format (key.upper (), msg), file=self._logfile)
        return

    ############################################################

    def _api (self, verb, req, params={}, doit=True):
        """Send an API request to the Trello server.

        See 'request' Python module for details.

        :param verb: 'GET', 'POST', 'DELETE', or 'PUT'
        :param req: API request (e.g. '/members/me/boards')
        :param params: query-string elements
        :param doit: True if the API call must be done
        :return: API reply
        """

        url = self._apiurl + '/' + req
        params ['key'] = self._apikey
        params ['token'] = self._token

        self._d ('api', '{} {} params={}'.format (verb, url, params))

        if doit:
            r = self._session.request (verb, url, params=params)

            self._d ('api', 'got {} {}'.format (r.status_code, r.reason))

            if r.status_code != 200:
                raise RuntimeError ('Server eror {} ({})\n{}'.
                                    format (r.status_code, r.reason, r.text))

            self._d ('json', '{} {}'.format (req, json.dumps (r.json ())))

        else:
            r = None

        return r

    ############################################################

    def _fetch_all_boards (self):
        """Fetch all my boards

        :return: nothing
        """

        if self._boards is None:
            r = self._api ('GET', '/members/me/boards')
            self._boards = r.json ()
        return

    ############################################################

    def boards (self):
        """Return the list of all (non closed) board names

        :return: list of board names
        """

        self._fetch_all_boards ()
        l = []
        for j in self._boards:
            if not j ['closed']:
                l.append (j ['name'])
        return l

    ############################################################

    def select_board (self, name):
        """Select a board

        Select a Trello board and fetch all associated objects
        lists, cards, checklists, labels)

        :param name: board name or None for the default board
        :return: selected (named or default) board name
        """

        def sortpos (l):
            return sorted (l, key=lambda i: i['pos'])

        # use board name or a default one
        if name is None:
            name = self._defaultboard

        # already loaded?
        if name == self._curname:
            return

        self._id = {}
        self._shid = {}

        # check board name
        self._fetch_all_boards ()
        self._curbid = None
        for j in self._boards:
            if name == j ['name']:
                self._curbid = j ['id']
                break

        if self._curbid is None:
            raise RuntimeError ("Board '{}' not found\nAvailable boards: {}"
                        .format (name, ' '.join (self.boards ())))

        # get all lists
        r = self._api ('GET', '/boards/' + str (self._curbid) + '/lists')
        self._lists = sortpos (r.json ())
        self._index_by_id ('L', self._lists)

        # get all cards
        r = self._api ('GET', '/boards/' + str (self._curbid) + '/cards')
        self._cards = sortpos (r.json ())
        self._index_by_id ('C', self._cards)

        # get all checklists
        r = self._api ('GET', '/boards/' + str (self._curbid) + '/checklists')
        self._checklists = sortpos (r.json ())
        self._index_by_id ('K', self._checklists)

        # get all labels
        r = self._api ('GET', '/boards/' + str (self._curbid) + '/labels')
        self._labels = r.json ()

        # remember board name
        self._curname = name

        return self._curname

    ############################################################

    def _index_by_id (self, typ, json):
        """Fill the id/shid array with objects in json

        For each Trello id, find a shorter id (last 3 or more
        characters of the Trello id), store it in _shid array
        and associate it to the object in the provided json object.

        Short ids are useful only for objects exported with an
        id in the bd output.

        :param typ: 'L' (list), 'C' (card), 'K' (checklist)
        :param json: json object to be completed with 'type' and 'shid' attrs
        :return: nothing
        """

        for j in json:
            if 'id' in j:
                id = j ['id']

                # find a short-id suitable for the complete id
                i = -3
                while id [i:] in self._shid and id [i:] != id:
                    i = i-1
                shid = id [i:]

                # remember the id associated to the short-id
                self._shid [shid] = id

                # remember the short-id associated to the object
                j ['type'] = typ                # 'L', 'C', 'K'
                j ['shid'] = shid

                # keep this object indexed by the full id
                self._id [id] = j
        return

    ############################################################

    def _id2shid (self, id):
        """Return the short id given the Trello id

        :param id: the Trello id
        :return: short-id
        """
        if id not in self._id:
            raise RuntimeError ('Internal error: id {} not found'.format (id))
        return self._id [id]['shid']

    ############################################################

    def _get_deco (self, json):
        # TODO add member and reminder
        """Return decoration (due, labels) to add to a card name

        :param json: object for which the decoration is displayed
        :return: string to add to name
        """
        r = ''
        if 'labels' in json and len (json ['labels']) > 0:
            lbl = []
            for l in json ['labels']:
                lbl.append (l ['name'])
            r += ' [' + ' '.join (lbl) + ']'
        if 'due' in json and json ['due'] is not None:
            r += ' [due: ' + json ['due'] + ']'
        return r

    ############################################################

    def bd_fetch (self):
        """Return a BD string representing the current board

        :return: tuple (n obj, bd text)
        """

        def bdappend (nsp, txt):
            """append an indented text to the bd variable"""
            bd.append (nsp * ' ' + txt)
            return

        if self._curname is None:
            raise RuntimeError ('No board selected')

        now = datetime.datetime.now ()
        nobj = 0

        # provide a comment at beginning of file
        bd = []
        bdappend (0, '# Board {}'.format (self._curname))
        bdappend (0, '# Exported by bdrello v{} on {}'.
                                        format (BDRELLO_VERSION, now))
        lbl = []
        for l in self._labels:
            lbl.append (l ['name'])
        bdappend (0, '# Available labels: {}'.format (', '.join (lbl)))

        # for all lists
        for l in self._lists:
            if not l ['closed']:
                bdappend (0, '')

                lid = l ['id']
                shid = self._id2shid (lid)
                bdappend (0, 'L({}) {}'.format (shid, l ['name']))
                nobj += 1

                # for all cards in the board (including those not in this list)
                for c in self._cards:
                    if not c ['closed'] and c ['idList'] == lid:
                        cid = c ['id']
                        shid = self._id2shid (cid)
                        deco = self._get_deco (c)
                        bdappend (2, 'C({}) {}{}'.
                                        format (shid, c ['name'], deco))
                        nobj += 1

                        # print desc
                        desc = c ['desc']
                        if desc != '':
                            for s in desc.split ('\n'):
                                bdappend (6, s)

                        # print checklist
                        for k in self._checklists:
                            if k ['idCard'] == cid:
                                shid = self._id2shid (k ['id'])
                                bdappend (4, 'K({}) {}'.
                                            format (shid, k ['name']))
                                nobj += 1
                                for i in k ['checkItems']:
                                    state = ' '
                                    if i ['state'] == 'complete':
                                        state = 'x'
                                    deco = self._get_deco (k)
                                    bdappend (6, '[{}] {}{}'.
                                            format (state, i ['name'], deco))
                                    nobj += 1
        return (nobj, '\n'.join (bd))

    ############################################################

    def diff (self, bd):
        """Find differences between the BD and the current Trello board

        Given a BD text or file, compute the differences from the
        Trello board. Differences are provided as a dict {bd, mod}
        where:
        - bd is the original BD without all identical fields
        - mod are the ids to be removed

        :param bd: file or text 
        :return: dict {'bd', 'mod'}
        """

        # check that boardname is already read
        if self._curname is None:
            raise RuntimeError ('No board selected')

        # create a list [(labelname id) ...]
        labels = [(j ['name'], j ['id']) for j in self._labels]

        # parse BD text or file
        br = BDReader ()
        pbd = br.parse (bd, self._shid, self._id, labels)

        self._d ('bd', json.dumps (pbd))

        #
        # first pass:
        # - mark all ids (in json) still in the new bd text
        # - removes non modified items (names, due, etc.) from the bd
        # - set 'pos' attribute identical to the original one for the 2nd pass
        #

        # lists
        lpos = 0
        for l in pbd:
            lid        = l ['id']
            lname      = l ['name']

            if lid != 'new':
                self._id [lid]['seen'] = True
                orgpos = self._id [lid]['pos']
                if orgpos > lpos:
                    l ['orgpos'] = orgpos
                    lpos = orgpos
                if lname == self._id [lid]['name']:
                    del l ['name']

            # cards
            cpos = 0
            for c in l ['C']:
                cid        = c ['id']
                cname      = c ['name']
                desc       = c ['desc']
                due        = c ['due']
                idLabels   = c ['idLabels']

                if cid != 'new':
                    self._id [cid]['seen'] = True
                    orgpos = self._id [cid]['pos']
                    if orgpos > cpos:
                        c ['orgpos'] = orgpos
                        cpos = orgpos
                    if cname == self._id [cid]['name']:
                        del c ['name']
                    if desc == self._id [cid]['desc']:
                        del c ['desc']
                    if due == self._id [cid]['due']:
                        del c ['due']
                    if lid != self._id [cid]['idList']:
                        c ['idList'] = lid      # may be 'new'

                    orglbl = self._id [cid]['idLabels']
                    orglbl.sort ()

                    if c ['idLabels'] == orglbl:
                        del c ['idLabels']

                # checklists
                kpos = 0
                for k in c ['K']:
                    kid        = k ['id']
                    kname      = k ['name']

                    # Hack!  Since moving checklist between cards is
                    # not supported by the Trello API, we do "as if"
                    # it was a new one (=> we will delete the old
                    # one and create a new one)
                    if kid != 'new' and cid != self._id [kid]['idCard']:
                        k ['id'] = 'new'
                        kid = 'new'


                    if kid != 'new':
                        self._id [kid]['seen'] = True
                        orgpos = self._id [kid]['pos']
                        if orgpos > kpos:
                            k ['orgpos'] = orgpos
                            kpos = orgpos
                        if kname == self._id [kid]['name']:
                            del k ['name']

                    # items for this checklist
                    ipos = 0
                    for cki in k ['I']:
                        iname    = cki ['name']
                        state    = cki ['state']
                        cki ['id'] = 'new'

                        # check if item is already there
                        if kid != 'new':
                            for oitem in self._id [kid]['checkItems']:
                                if iname == oitem ['name']:
                                    # old item found
                                    cki ['id'] = oitem ['id']
                                    oitem ['seen'] = True
                                    del cki ['name']
                                    orgpos = oitem ['pos']
                                    if orgpos > ipos:
                                        cki ['orgpos'] = orgpos
                                        ipos = orgpos
                                    if state == oitem ['state']:
                                        del cki ['state']
                                    break

        #
        # second pass:
        # - confirm or get a position for each item (list, card, checkL, item)
        #

        self._assign_pos (pbd)
        for l in pbd:
            self._assign_pos (l ['C'])
            for c in l ['C']:
                self._assign_pos (c ['K'])
                for k in c ['K']:
                    self._assign_pos (k ['I'])

        #
        # third pass:
        # - fetch all id to delete
        #

        delete = {'I': [], 'K': [], 'C': [], 'L': []}
        for ck in self._checklists:
            ckid = ck ['id']
            for it in ck ['checkItems']:
                if 'seen' not in it:
                    delete ['I'].append ( (ckid, it ['id']) )
            if 'seen' not in ck:
                delete ['K'].append (ckid)
        for c in self._cards:
            if 'seen' not in c:
                delete ['C'].append (c ['id'])
        for l in self._lists:
            if 'seen' not in l:
                delete ['L'].append (l ['id'])

        # pack both informations into a dictionary
        diff = {'bd': pbd, 'del': delete }

        self._d ('diff', json.dumps (diff))

        return diff

    ############################################################

    def _assign_pos (self, lst):
        """Assign positions to each element of list lst

        Each element may have a 'orgpos' attribute which
        is the original position. These values are deleted
        in output.
        If a position different from orgpos must be assigned
        to the element, a 'pos' attribute is added.

        :param lst: list of objects (lists, cards, checklists, items)
        :return: nothing
        """

        def assign (lst, firstidx, lastidx, firstpos, lastpos):
            """Fill holes in positions"""

            n = lastidx - firstidx
            incr = float (lastpos - firstpos) / (n + 1)
            if incr > 10:
                incr = int (incr)       # avoid unuseful floats
            for x in range (firstidx, lastidx):
                firstpos += incr
                lst [x]['pos'] = firstpos
            return

        curpos = 0                      # current position
        last = 0                        # index of first unknown position
        l = len (lst)
        for i in range (0,l):
            if 'orgpos' in lst [i]:
                orgpos = lst [i]['orgpos']
                del lst [i]['orgpos']
                if i - last > 0:
                    assign (lst, last, i, curpos, orgpos)
                last = i+1
                curpos = orgpos
        if l - last > 0:
            assign (lst, last, l, curpos, curpos + 2**16 * (l-last))
        return

    ############################################################

    def create_temp (self, bd):
        """Create and initialize temporary file for the BD

        :param bd: BD to store in the file
        :return: path of temporary file
        """
        try:
            d = tempfile.NamedTemporaryFile (mode='w', suffix='.bd', delete=False)
            print (bd, file=d.file)
            d.file.close ()
        except Exception as e:
            raise RuntimeError ('Cannot write to temporary file {}'.e)
        return d.name

    ############################################################

    def backup (self, src, dst):
        """Copy the source file in the backup directory

        Note: if not backup directory is not defined, this
        method does not do anything.

        :param src: original file path
        :param dst: file to be created in the backup directory
        :return: nothing
        """
        if self._backupdir is not None:
            new = self._backupdir + '/' + dst
            try:
                shutil.copyfile (src, new)
            except Exception as e:
                raise RuntimeError ('Cannot back-up {} to {}\n{}'.
                                format (src, new, e))
        return

    ############################################################

    def editor (self):
        """Return the editor.

        Rules for editor:
        1- use the default editor specified in the configuration file
        2- use the EDITOR environment variable
        3- use 'vi'

        :return: editor
        """
        if self._editor is not None:
            editor = self._editor
        elif (editor := os.getenv ("EDITOR", None)) is not None:
            pass
        else:
            editor = BDrello.DEFAULT_EDITOR

        return editor

    ############################################################

    def commit (self, diff, doit):
        """Commit all changes

        :param diff: differences (see the 'diff' method)
        :param doit: True if the changes must be sent to the Trello server
        :return: tuple (nadd, nmod, ndel) with number of changes
        """

        def init_params (src, attr):
            params = {}
            for f in attr:
                if f in src:
                    params [f] = src [f]
            return params

        if 'bd' not in diff:
            raise RuntimeError ("Bad format for 'diff' parameter : no 'bd'")
        if 'del' not in diff:
            raise RuntimeError ("Bad format for 'diff' parameter : no 'del'")
        if ('bd' not in diff) or ('del' not in diff):
            raise RuntimeError ("Bad format for 'diff' parameter")

        nadd = 0
        nmod = 0
        ndel = 0

        #
        # First pass : create or modify all new data
        #

        for l in diff ['bd']:
            lid = l ['id']
            fields = ['name', 'pos']
            params = init_params (l, fields)
            if lid == 'new':
                params ['idBoard'] = self._curbid
                r = self._api ('POST', '/lists', params=params, doit=doit)
                if doit:
                    if 'id' not in r.json ():
                        raise RuntimeError ('no id from list {} creation'.format (l['name']))
                    lid = r.json () ['id']
                nadd += 1

            elif len(params) > 0:               # modify existing list
                path =  '/lists/{}'.format (lid)
                r = self._api ('PUT', path, params=params, doit=doit)
                nmod += 1

            for c in l ['C']:
                cid = c ['id']
                if 'idList' in c and c ['idList'] == 'new':
                    c ['idList'] = lid
                fields = ['name', 'desc', 'due', 'pos', 'idLabels', 'idList']
                params = init_params (c, fields)
                if cid == 'new':
                    params ['idList'] = lid
                    r = self._api ('POST', '/cards', params=params, doit=doit)
                    if doit:
                        if 'id' not in r.json ():
                            raise RuntimeError ('no id from card {} creation'.format (c['name']))
                        cid = r.json () ['id']
                    nadd += 1

                elif len(params) > 0:           # modify existing card
                    path =  '/cards/{}'.format (cid)
                    r = self._api ('PUT', path, params=params, doit=doit)
                    nmod += 1

                for k in c ['K']:
                    kid = k ['id']
                    fields = ['name', 'pos', 'idCard']
                    params = init_params (k, fields)
                    if kid == 'new':
                        params ['idCard'] = cid
                        r = self._api ('POST', '/checklists', params=params, doit=doit)
                        if doit:
                            if 'id' not in r.json ():
                                raise RuntimeError ('no id from checlist {} creation'.format (k['name']))
                            kid = r.json () ['id']
                        nadd += 1

                    elif len(params) > 0:       # modify existing checklist
                        path =  '/checklists/{}'.format (kid)
                        r = self._api ('PUT', path, params=params, doit=doit)
                        nmod += 1

                    for cki in k ['I']:
                        ckid = cki ['id']
                        fields = ['name', 'pos', 'due', 'state']
                        params = init_params (cki, fields)

                        if ckid == 'new':
                            params ['idCard'] = cid
                            del params ['state']
                            params ['checked'] = 'true' if cki['state'] == 'complete' else 'false'

                            path = '/checklists/{}/checkItems'.format (kid)
                            r = self._api ('POST', path, params=params, doit=doit)
                            if doit:
                                if 'id' not in r.json ():
                                    raise RuntimeError ('no id from checkitem {} creation'.format (cki['name']))
                            nadd += 1

                        elif len(params) > 0:   # modify existing checkitem
                            path = '/cards/{}/checkItem/{}'.format (cid, ckid)
                            r = self._api ('PUT', path, params=params, doit=doit)
                            nmod += 1

        #
        # Second pass : delete old ids
        #

        for (ckid, itid) in diff ['del']['I']:
            path = '/checklists/{}/checkItems/{}'.format (ckid, itid)
            r = self._api ('DELETE', path, doit=doit)
            ndel += 1

        for ckid in diff ['del']['K']:
            path = '/checklists/{}'.format (ckid)
            r = self._api ('DELETE', path, doit=doit)
            ndel += 1

        for cid in diff ['del']['C']:
            path = '/cards/{}'.format (cid)
            r = self._api ('DELETE', path, doit=doit)
            ndel += 1

        for lid in diff ['del']['L']:
            params = {'value': "true"}
            # archive the list
            path = '/lists/{}/closed'.format (lid)
            r = self._api ('PUT', path, params=params, doit=doit)
            ndel += 1

        return (nadd, nmod, ndel)

##############################################################################
# BDReader
##############################################################################

class BDReader:
    """\
        Specialized class to parse a bd text

        The only method is reader (iterator) which returns :
            - the board as a list of Trello lists [L1 L2 ...]
            - each list Li is a dictionary containing:
                - id : original id or 'new'
                - name : name of list
                - C : list of Trello cards [C1 C2 ...]
            - each card Ci is a dictionary containing:
                - id : original id or 'new'
                - name : name of card
                - desc : description (multiple lines)
                - due : date & time
                - idLabels : sorted list of label ids
                - K : list of Trello checklists [K1 K2 ...]
            - each checklist Ki is a dictionary containing:
                - id : original id or 'new'
                - name : name of checklist
                - I : list of dictionaries [I1 I2...]
            - each checkitem Ii is a dictionary containing:
                - name : name of item
                - state : 'complete'|'incomplete'
    """

    ############################################################

    def __init__ (self):

        # current state of reader (see the 'parse' method)
        self._board     = []
        self._list      = {}
        self._card      = {}
        self._checklist = {}

        # regular expressions used by the parser
        self._re_comment = re.compile (r' *#.*')
        self._re_empty   = re.compile (r' *$')
        self._re_letter  = re.compile (r' *([LCK])\(([0-9a-f]*)\) +(.*)')
        self._re_item    = re.compile (r' *\[(.)\] +(.*)')
        self._re_desc    = re.compile (r'( *)([^ ].*)')

        self._re_brack   = re.compile (r'(.*[^ ]) *\[([^\]]+)\] *$')

        self._id = {}

        # states of the automaton
        self._S_START = 1               # anywhere, except...
        self._S_C     = 2               # ... in card description
        self._S_K     = 3               # ... or in checklist items

        self._state   = self._S_START

        # current line number
        self._lineno = 0

    ############################################################

    def _grmbl (self, msg):
        raise RuntimeError ('Error parsing bd text in line {}\n{}'.
                                    format (self._lineno, msg))

    ############################################################

    def _close_checklist (self):
        if len (self._checklist) > 0:
            self._card ['K'].append (self._checklist)
            self._checklist = {}
        return

    ############################################################

    def _close_card (self):
        self._close_checklist ()
        if len (self._card) > 0:
            # remove empty lines at end of desc
            desc = self._card ['desc']
            for i in range (len (desc)-1,-1,-1):
                if desc [i] != '':
                    break
                else:
                    del (desc [i])
            self._card ['desc'] = '\n'.join (desc)
            self._list ['C'].append (self._card)
        self._card = {}
        return

    ############################################################

    def _close_list (self):
        self._close_card ()
        if len (self._list) > 0:
            self._board.append (self._list)
        self._list = {}
        return

    ############################################################

    def _parse_id (self, shid, letter):
        if shid == '':
            r = 'new'
        elif shid in self._id:
            self._grmbl ('id {} already used'.format (shid))
        elif shid in self._shid:
            fullid = self._shid [shid]
            l = self._fullid [fullid]['type']
            if l != letter:
                self._grmbl ('id {} is not a {} id'.format (shid, letter))
            r = fullid
            self._id [shid] = True
        else:
            self._grmbl ('id {} not found'.format (shid))
            raise RuntimeError
        return r

    ############################################################

    def _parse_deco (self, title):
        """Parse [due: ...] and [labels] in a bd title

        :param title: name and decoration
        :return: a tuple (name, due, idLabels)
        """

        name = title
        due = None
        idLabels = []
        while (m := self._re_brack.match (name)):
            name = m.group (1)
            brack = m.group (2).split ()
            if brack [0] == 'due:':
                if len (brack) != 2:
                    self._grmbl ('invalid "due" clause {}'.format (title))
                due = brack [1]
            else:
                for l in brack:
                    idFound = None
                    for (lname, id) in self._labels:
                        if l == lname:
                            idFound = id
                            break
                    if idFound is None:
                        self._grmbl ('invalid label name {}'.format (l))
                    idLabels.append (idFound)
        idLabels.sort ()
        return (name, due, idLabels)

    ############################################################

    def parse (self, it, shid, id, labels):
        """Read and parse bd specification from an iterator
        (a file or a list). Checks id and label validity.

        :param it: text or file
        :param shid: array indexed by short-id giving the full-id
        :param id: all objects in the Trello board indexed by full-ids
        :param labels: list of tuples (label-name, label-full-id)
        :return: the bd without all identical fields removed
        """

        self._board     = []
        self._list      = {}
        self._card      = {}
        self._checklist = {}

        self._id = {}

        self._shid = shid
        self._fullid = id
        self._labels = labels                   # [(name,id) ...]

        state = self._S_START
        self._lineno = 0
        for line in it:
            self._lineno += 1
            line = line.expandtabs (8)

            m0 = self._re_comment.match (line)
            m1 = self._re_empty.match (line)
            m2 = self._re_letter.match (line)
            m3 = self._re_item.match (line)
            m4 = self._re_desc.match (line)

            if m0:                              # comment
                pass

            elif m1 and state == self._S_C:     # empty line in desc
                self._card ['desc'].append ('')

            elif m1:                            # empty line elsewhere
                pass

            elif m2:                            # L/C/K
                letter = m2.group (1)
                shid   = m2.group (2)
                title  = m2.group (3)

                id = self._parse_id (shid, letter)
                (name, due, idLabels) = self._parse_deco (title)

                if letter == 'L':
                    state = self._S_START
                    self._close_list ()
                    self._list = {'id': id, 'name': name, 'C': []}
                    if due is not None:
                        self._grmbl ('invalid [due: ...]')
                    if len (idLabels) > 0:
                        self._grmbl ('invalid [labels]')

                elif letter == 'C':
                    state = self._S_C
                    self._close_card ()
                    self._card = {'id': id, 'name': name, 'desc': [],
                                    'due': due, 'idLabels': idLabels, 'K': []}
                    desc_nspaces = None

                elif letter == 'K':
                    state = self._S_K
                    self._close_checklist ()
                    self._checklist = {'id': id, 'name': name, 'I': []}
                    if due is not None:
                        self._grmbl ('invalid [due: ...]')
                    if len (idLabels) > 0:
                        self._grmbl ('invalid [labels]')

                else:
                    self._grmbl ('invalid letter {}'.format (letter))

            elif m3 and state == self._S_K:     # checklist item
                complete = m3.group (1)
                title = m3.group (2)
                (name, due, idLabels) = self._parse_deco (title)
                if due is not None:
                    self._grmbl ('invalid [due: ...]')
                if len (idLabels) > 0:
                    self._grmbl ('invalid [labels]')
                if complete != ' ':
                    complete = 'complete'
                else:
                    complete = 'incomplete'
                self._checklist ['I'].append ({'name': name, 'state': complete})

            elif m4 and state == self._S_C:     # card description
                nspaces = len (m4.group (1))
                desc = m4.group (2)

                # if first desc line, remember the number of spaces at beginning
                if desc_nspaces is None:
                    desc_nspaces = nspaces
                
                # for all desc, add relevant number of spaces
                desc = (desc_nspaces - nspaces) * ' ' + desc
                self._card ['desc'].append (desc)

            else:
                self._grmbl ('invalid line')

        self._close_list ()

        return self._board

##############################################################################
# main program
##############################################################################

verbose = False

def verb(msg):
    if verbose:
        print (msg, file=sys.stderr)

def main ():
    parser = argparse.ArgumentParser (description='bdrello')
    parser.add_argument ('-V', '--version', action='store_true',
            help='print version and exit')
    parser.add_argument ('-v', '--verbose', action='store_true',
            help='print additional information verbose')
    parser.add_argument ('-d', '--debug', action='store',
            help='comma separated list: config,api,json,bd,diff')
    parser.add_argument ('-l', '--log-file', action='store',
            help='file to use for debug messages')
    parser.add_argument ('-c', '--config-file', action='store',
            help='configuration file')
    parser.add_argument ('-b', '--board-name', action='store',
            help='board name')
    parser.add_argument ('-n', '--do-not-commit', action='store_true',
            help='do not apply modifications (use with -d api or more)')
    parser.add_argument ('action', action='store',
            nargs='?', default='g',
            choices=['g', 'get', 'p', 'put', 'e', 'edit', 'l', 'list'],
            help="g(et) from Trello, p(ut) to Trello, e(dit), l(ist) boards")

    args = parser.parse_args ()

    debug       = [] if args.debug is None else args.debug.split (',')
    version     = args.version
    config_file = args.config_file
    log_file    = args.log_file
    board_name  = args.board_name
    dont        = args.do_not_commit

    if version:
        print ('bdrello version {}'.format (BDRELLO_VERSION))
        sys.exit (0)

    global verbose
    verbose    = args.verbose

    action     = args.action [0]

    try:
        try:
            c = BDrello (config_file, debug, log_file)
        except RuntimeError as e:
            print (e, file=sys.stderr)
            sys.exit (1)

        match action:
            case 'l':
                print ('\n'.join (c.boards ()))

            case 'g':
                curname = c.select_board (board_name)
                (nobj, bd) = c.bd_fetch ()
                print (bd)
                verb ('objects: {}'.format (nobj))

            case 'p':
                curname = c.select_board (board_name)
                diff = c.diff (sys.stdin)
                (nadd, nmod, ndel) = c.commit (diff, not dont)
                verb ('objects added:{}, modified:{} deleted:{}'.
                                    format (nadd, nmod, ndel))

            case 'e':
                curname = c.select_board (board_name)
                (nobj, bd) = c.bd_fetch ()
                try:
                    tmp = c.create_temp (bd)
                    c.backup (tmp, curname + '.1.bd')
                    editor = c.editor ()
                    try:
                        p = subprocess.run ([editor, tmp])
                        p.check_returncode ()
                    except Exception as e:
                        raise RuntimeError ('Cannot execute {} {}\n{}'.
                                                format (editor, tmp, e))
                    c.backup (tmp, curname + '.2.bd')
                    with open (tmp) as f:
                        diff = c.diff (f)
                except Exception as e:
                    raise RuntimeError (e)
                finally:
                    os.unlink (tmp)

                (nadd, nmod, ndel) = c.commit (diff, not dont)
                if nadd + nmod + ndel == 0:
                    print ('No change')
                verb ('objects added={}, modified={} deleted={}'.
                                    format (nadd, nmod, ndel))

    except RuntimeError as e:
        print (e, file=sys.stderr)
        sys.exit (1)

    except KeyboardInterrupt:
        print ('Aborted', file=sys.stderr)
        sys.exit (1)

    # print ('{}'.format (r))

if __name__ == '__main__':
    main ()
    sys.exit (0)

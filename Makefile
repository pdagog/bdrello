bdrello.pdf: bdrello.man
	tbl $< | groff -man > $@

bdrello.man: bdrello.md
	pandoc \
	    --standalone \
	    --from markdown+simple_tables \
	    --to man \
	    $< > $@
